
#include "DHT.h"
#define DHTPIN 13
#define DHTTYPE DHT22 //#define DHTTYPE DHT11 (pour un capteur DHT11)

DHT dht(DHTPIN, DHTTYPE);
 
float h = 0;
float t = 0;
float sethl = 0;
float settl = 0;
float sethh = 0;
float setth = 0;

#include <LiquidCrystal.h>


LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  dht.begin();
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(A0,INPUT_PULLUP);
  pinMode(A1,INPUT_PULLUP);
  pinMode(A2,INPUT_PULLUP);
  pinMode(A3,INPUT_PULLUP);
  pinMode(A4,INPUT_PULLUP);
  pinMode(7,OUTPUT);
  pinMode(8,OUTPUT);
}

void loop() {
   h = dht.readHumidity();
   t = dht.readTemperature(); 
   onoffrelay();
   printht();
   if(digitalRead(A0) == LOW)loopset();  //even set button
   if(digitalRead(A4) == LOW){sethl = 0;settl = 0;sethh = 0;setth = 0;}  //button restart
}

void printDigits(byte digits){
  // utility function for digital clock display: prints colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits,DEC);   
}

void onoffrelay(){
     if(sethl > sethh){
       while(true){
         lcd.setCursor(0, 0);
         lcd.print( "errors Moisture ");
         lcd.setCursor(0, 1);
         lcd.print( "LOW > HIGH      ");
         if(digitalRead(A0) == LOW)break;  //even set button
       }
     }
     else if(settl > setth){
       while(true){
         lcd.setCursor(0, 0);
         lcd.print( "errors Temp     ");
         lcd.setCursor(0, 1);
         lcd.print( "LOW > HIGH      ");
         if(digitalRead(A0) == LOW)break;  //even set button
       }
     }
     else{
        if(t <= settl){
           digitalWrite(7, HIGH);  // open heater
         }
         if(t >= setth){
           digitalWrite(7, LOW); // close heater
         }
         if(h <= sethl){
            digitalWrite(8, LOW); // open fan
         }
         if(h >= sethh){
           digitalWrite(8, HIGH); //close fan
         }
        if(settl == 0 || setth == 0){
          digitalWrite(7, LOW); // close heater
        }
        if(sethl == 0 || sethh == 0){
          digitalWrite(8, LOW); // open fan
        }
     }    
}

void printht(){
    if (isnan(t) || isnan(h)){
      lcd.print( "sensor error");
    }    
    else{
        Serial.print("Temperature ");  
        Serial.print(t);
        Serial.print(" *c \t");
        Serial.print("Moisture ");  
        Serial.print(h);
        Serial.print("% \t");                
        Serial.print("\n");

        //---------------* show state Temp and Mositure *------------
        lcd.setCursor(0, 0);
        lcd.print("Moisture:");
        lcd.setCursor(9,0);
        lcd.print(h);
        lcd.setCursor(14,0);
        lcd.print("% ");
        lcd.setCursor(0, 1);
        lcd.print("Temp:    ");
        lcd.setCursor(9,1);
        lcd.print(t);
        lcd.setCursor(14,1);
        lcd.print("*C");
        
        delay(1000);
        
        //----------* state heater * --------------------------------
        if(digitalRead(7) == LOW){           
          lcd.setCursor(0, 0);
          lcd.print( "heater close    ");  //print state heater to lcd
        }
        if(digitalRead(7) == HIGH){
          lcd.setCursor(0, 0);
          lcd.print( "heater open     ");  //print state heater to lcd
        }
        
        //----------* state fan * ------------------------------------
        if(digitalRead(8) == LOW){
          lcd.setCursor(0, 1);
          lcd.print( "fan close       ");  //print state fan to lcd
        }
        if(digitalRead(8) == HIGH){
          lcd.setCursor(0, 1);
          lcd.print( "fan open        ");  //print state fan to lcd
        }
        
        delay(1000);
    } 
}

void loopset() {
   while(true){
     lcd.setCursor(0,0);
     lcd.print("press + Moisture");
     lcd.setCursor(0,1);
     lcd.print("press - Temp    "); 
     if(digitalRead(A1) == LOW){loopseth();break;}  //button +
     if(digitalRead(A2) == LOW){loopsett();break;}  //button -
     if(digitalRead(A3) == LOW)break;  //button start
   }
}

void loopseth(){
  while(true){
     lcd.setCursor(0,0);
     lcd.print("Set Moisture    ");
     lcd.setCursor(0,1);
     lcd.print("low:");
     lcd.setCursor(4,1);
     lcd.print(sethl);
     lcd.setCursor(8,1);
     lcd.print("         ");
     delay(100);
     lcd.setCursor(4,1);
     lcd.print("    ");
     delay(100);
     if(digitalRead(A0) == LOW){ //button set
       while(true){ 
           lcd.setCursor(0,1);
           lcd.print("high:");
           lcd.setCursor(5,1);
           lcd.print(sethh);
           lcd.setCursor(9,1);
           lcd.print("         ");
           delay(100);
           lcd.setCursor(5,1);
           lcd.print("    ");
           delay(100);
           if(digitalRead(A3) == LOW){break;} //button start
           if(digitalRead(A1) == LOW){sethh++;}  //button +
           if(digitalRead(A2) == LOW){sethh--;}  //button -
           if(digitalRead(A4) == LOW)sethh=0;  //button restart
       }
     }
     if(digitalRead(A1) == LOW){sethl++;}  //button +
     if(digitalRead(A2) == LOW){sethl--;}  //button -
     if(digitalRead(A3) == LOW)break;  //button start
     if(digitalRead(A4) == LOW)sethl=0;  //button restart
   }
}
void loopsett(){
  while(true){
     lcd.setCursor(0,0);
     lcd.print("Set Temp        ");
     lcd.setCursor(0,1);
     lcd.print("low:");
     lcd.setCursor(4,1);
     lcd.print(settl);
     lcd.setCursor(8,1);
     lcd.print("         ");
     delay(100);
     lcd.setCursor(4,1);
     lcd.print("    ");
     delay(100);
     if(digitalRead(A0) == LOW){ //button set
       while(true){ 
           lcd.setCursor(0,1);
           lcd.print("high:");
           lcd.setCursor(5,1);
           lcd.print(setth);
           lcd.setCursor(9,1);
           lcd.print("         ");
           delay(100);
           lcd.setCursor(5,1);
           lcd.print("    ");
           delay(100);
           if(digitalRead(A3) == LOW){break;} //button start
           if(digitalRead(A1) == LOW){setth++;}  //button +
           if(digitalRead(A2) == LOW){setth--;}  //button -
           if(digitalRead(A4) == LOW)setth=0;  //button restart
       }
     }
     if(digitalRead(A1) == LOW){settl++;}  //button +
     if(digitalRead(A2) == LOW){settl--;}  //button -
     if(digitalRead(A3) == LOW)break;  //button start
     if(digitalRead(A4) == LOW)settl=0;  //button restart
   }
}
